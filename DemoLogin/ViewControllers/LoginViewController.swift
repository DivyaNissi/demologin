//
//  LoginViewController.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 14/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit

let appDelegate = UIApplication.shared.delegate as? AppDelegate
let sceneDelegate = UIApplication.shared.delegate as? SceneDelegate

final class LoginViewController: UIViewController {
    @IBOutlet var userFields: [UITextField]!
    let loginViewModel = LoginViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureLoginView()
        // Do any additional setup after loading the view.
    }
    
    private func configureLoginView() {
        let fieldInfo = loginViewModel.fieldsInfoArray
        
        for (index, info) in fieldInfo.enumerated() {
            
            userFields[index].placeholder = info.placeHolder
            userFields[index].tag = info.fieldType.rawValue
            userFields[index].delegate = self
            
        }
        
    }
    
    
    @IBAction func actionLogin(_ sender: UIButton) {
        view.endEditing(true)
        
        guard !(userFields.first?.text?.isEmpty ?? true) else {
            showAlertviewController(messageName: "Please enter valid user ID")
            return
        }
        
        guard !(userFields.last?.text?.isEmpty ?? true) else {
            showAlertviewController(messageName: "Please enter valid password")
            return
        }
        
        guard loginViewModel.isValidLogin(id: Int16(userFields.first?.text ?? "") ?? 0, password: userFields.last?.text ?? "") else {
            
            showAlertviewController(messageName: "No user exists with the entered credentials.")
            return
        }
        
        print("login Successful")
        showImagesViewController()
    }
    
    private func showImagesViewController() {
        let imagesController = ImagesViewController.instantiate()
        UIApplication.shared.windows.first?.rootViewController = imagesController
        UIApplication.shared.windows.first?.makeKeyAndVisible()

    }
    @IBAction func actionSignUp(_ sender: UIButton) {
        view.endEditing(true)
        let registrationVC = RegistrationViewController.instantiate()
        present(registrationVC, animated: true, completion: nil)
    }
    
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}

extension LoginViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let field = FieldType(rawValue: textField.tag) {
            loginViewModel.updateField(fieldType: field, value: textField.text ?? "")
        }
    }
}
