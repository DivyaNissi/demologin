//
//  ImagesViewController.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 20/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit

final class ImagesViewController: UIViewController {
    @IBOutlet weak var infoTableView: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }
}

extension ImagesViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: ImageViewInfoCell.self), for: indexPath) as? ImageViewInfoCell {
            cell.titleLabel.text = "Test"
            cell.desLabel.text = "Test"
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    }
}

