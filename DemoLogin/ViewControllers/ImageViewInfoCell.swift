//
//  ImageViewInfoCell.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 20/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit

class ImageViewInfoCell: UITableViewCell {
    
    @IBOutlet weak var leftImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var desLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
