//
//  ViewController.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 13/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit

final class RegistrationViewController: UIViewController {
    @IBOutlet var userFields: [UITextField]!
    
    let registrationViewModel = RegistrationViewModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        configureSignUpView()
        
    }
    
    private func configureSignUpView() {
        let fieldInfo = registrationViewModel.fieldsInfoArray
        
        for (index, info) in fieldInfo.enumerated() {
            
            userFields[index].placeholder = info.placeHolder
            userFields[index].tag = info.fieldType.rawValue
            userFields[index].delegate = self
            
        }
        
    }
    
    @IBAction func actionSignUp(_ sender: UIButton) {
        view.endEditing(true)
        do {
            try  registrationViewModel.validateUserInfo()
            do {
                try registrationViewModel.saveUserInfoIntoCoreData()
                dismiss(animated: true, completion: nil)
            } catch let error as NSError {
                print("Unresolved error \(error), \(error.userInfo)")
            }
        } catch (let error) {
            print(error.localizedDescription)
            showAlertviewController(messageName: (error as! ValidationError).message)
        }
    }
    
}

extension RegistrationViewController: UITextFieldDelegate {
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        if let field = FieldType(rawValue: textField.tag) {
            registrationViewModel.updateField(fieldType: field, value: textField.text ?? "")
        }
    }
}
