//
//  User+CoreDataProperties.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 19/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import Foundation
import CoreData


extension User {
    
    @nonobjc public class func fetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }
    
    @NSManaged public var id: Int16
    @NSManaged public var name: String?
    @NSManaged public var password: String?
    @NSManaged public var email: String?
    @NSManaged public var phonenumber: String?
    
}
