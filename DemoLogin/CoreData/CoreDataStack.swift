//
//  CoreDataStack.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 19/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import Foundation
import CoreData

class CoreDataStack: NSObject {
    private let model: String
    init(modelName: String) {
        self.model = modelName
        
    }
    private lazy var storeContainer: NSPersistentContainer  = {
        
        let container = NSPersistentContainer(name: self.model)
        container.loadPersistentStores { (storeDescription, error) in
            if let errorDescription = error as NSError?{
                
                print(errorDescription.userInfo)
            }
        }
        return container
    }()
    
    lazy var managedContext: NSManagedObjectContext = {
        return self.storeContainer.viewContext
    }()
    
    func saveContext ()  throws {
        guard managedContext.hasChanges else { return }
        try managedContext.save()
        
    }
    
    
}
