//
//  Extensions.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 18/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import Foundation
import UIKit

enum AppStoryboard: String {
    
    case main       =   "Main"
    
    
    var instance: UIStoryboard {
        return UIStoryboard(name: self.rawValue, bundle: Bundle.main)
    }
    
    func viewController<T: UIViewController>(viewControllerClass: T.Type) -> T {
        let storyboardID = viewControllerClass.storyboardID
        return instance.instantiateViewController(withIdentifier: storyboardID) as! T
    }
}

extension Optional where Wrapped == String {
  @discardableResult  func validatedText(validationType: ValidatorType) throws -> String {
        let validator = VaildatorFactory.validatorFor(type: validationType)
        return try validator.validated(self ?? "")
    }
}





