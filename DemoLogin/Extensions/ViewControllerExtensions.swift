//
//  ViewControllerExtensions.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 06/12/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import Foundation
import UIKit

extension UIViewController {
    class var storyboardID: String {
        return "\(self)"
    }
    
    
    static func instantiate(fromStoryboard storyboard: AppStoryboard = .main ) -> Self {
        return storyboard.viewController(viewControllerClass: self)
    }
    
    
    func showAlertviewController(titleName: String = "", messageName: String, cancelButtonTitle: String = "OK", otherButtonTitles:[String] = [], selectionHandler: ((Int) -> Void)? = nil) {
        
        
        let alertController = UIAlertController(title: NSLocalizedString(titleName, comment: ""), message: NSLocalizedString(messageName, comment: ""), preferredStyle: .alert)
        let alertActionOk = UIAlertAction(title: NSLocalizedString(cancelButtonTitle, comment: ""), style: .cancel, handler: { alert in
            
            selectionHandler?(alertController.actions.firstIndex(of: alert)!)
        })
        otherButtonTitles.forEach {
            
            let alertAction = UIAlertAction(title: NSLocalizedString($0, comment: ""), style: .default, handler: { alert in
                selectionHandler?(alertController.actions.firstIndex(of: alert)!)
            })
            alertController.addAction(alertAction)
            
        }
        
        alertController.addAction(alertActionOk)
        UIApplication.topViewController()?.present(alertController, animated: true, completion: nil)
        
    }
}
