//
//  LoginViewModel.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 14/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit


enum FieldType: Int {
    
    case userID = 0
    case userName
    case mobileNumber
    case email
    case password
}

struct UserInfoModel {
    
    var placeHolder: String
    var fieldType: FieldType
    var enteredValue: String
    
}

struct UserModel {
    var name: String?
    var email: String?
    var userId: String?
    var phoneNumber: String?
    var password: String?
    mutating func updateUserId(userId: String) {
        self.userId = userId
    }
    mutating func updateUserName(name: String) {
        self.name = name
    }
    mutating func updateUserPhoneNumber(phoneNumber: String) {
        self.phoneNumber = phoneNumber
    }
    mutating func updateUserEmail(email: String) {
        self.email = email
    }
    mutating func updateUserPassword(password: String) {
        self.password = password
    }
}

class RegistrationViewModel: NSObject {
    
    let fieldsInfoArray: [UserInfoModel]
    private var userModel = UserModel()
    override init() {
        self.fieldsInfoArray =  {
            
            let userIDModel             = UserInfoModel(placeHolder: "Employee ID", fieldType: .userID, enteredValue: "")
            let userNameModel           = UserInfoModel(placeHolder: "Employee Name", fieldType: .userName, enteredValue: "")
            let userPhoneNumberModel    = UserInfoModel(placeHolder: "Phone Number", fieldType: .mobileNumber, enteredValue: "")
            let userEmailModel          = UserInfoModel(placeHolder: "Email", fieldType: .email, enteredValue: "")
            let passwordModel           = UserInfoModel(placeHolder: "Password", fieldType: .password, enteredValue: "")
            
            let fieldsArray = [userIDModel,userNameModel,userPhoneNumberModel, userEmailModel,passwordModel]
            return fieldsArray
        }()
        super.init()
    }
    
    func updateField(fieldType: FieldType, value: String) {
        switch fieldType {
        case .userID:
            userModel.updateUserId(userId: value)
        case .userName:
            userModel.updateUserName(name: value)
        case .mobileNumber:
            userModel.updateUserPhoneNumber(phoneNumber: value)
        case .email:
            userModel.updateUserEmail(email: value)
        case .password:
            userModel.updateUserPassword(password: value)
            
        }
    }
    
    func validateUserInfo() throws  {
        try userModel.userId.validatedText(validationType: ValidatorType.userId)
        try userModel.name.validatedText(validationType: ValidatorType.username)
        try userModel.phoneNumber.validatedText(validationType: ValidatorType.phoneNumber)
        try userModel.email.validatedText(validationType: ValidatorType.email)
        try userModel.password.validatedText(validationType: ValidatorType.password)
    }
    
    func saveUserInfoIntoCoreData() throws {
        guard let appDelegate = appDelegate else {
            return
        }
        let userDataModel           = User(context: appDelegate.stack.managedContext)
        userDataModel.email         = userModel.email
        userDataModel.name          = userModel.name
        userDataModel.id            = Int16(userModel.userId ?? "") ?? 0
        userDataModel.password      = userModel.password
        userDataModel.phonenumber   = userModel.phoneNumber
        try appDelegate.stack.saveContext()
        
    }
}


