//
//  LoginviewModel.swift
//  DemoLogin
//
//  Created by Divya Kothagattu on 14/11/19.
//  Copyright © 2019 Divya Kothagattu. All rights reserved.
//

import UIKit
import CoreData

class LoginViewModel: NSObject {
    
    let fieldsInfoArray: [UserInfoModel]
    private var userModel = UserModel()
    override init() {
        self.fieldsInfoArray =  {
            let userEmailModel          = UserInfoModel(placeHolder: "User ID", fieldType: .email, enteredValue: "")
            let passwordModel          = UserInfoModel(placeHolder: "Password", fieldType: .password, enteredValue: "")
            
            let fieldsArray = [userEmailModel,passwordModel]
            return fieldsArray
        }()
        super.init()
    }
    
    func updateField(fieldType: FieldType, value: String) {
        switch fieldType {
        case .userID:
            userModel.updateUserId(userId: value)
        case .userName:
            userModel.updateUserName(name: value)
        case .mobileNumber:
            userModel.updateUserPhoneNumber(phoneNumber: value)
        case .email:
            userModel.updateUserEmail(email: value)
        case .password:
            userModel.updateUserPassword(password: value)
            
        }
    }
    
    func isValidLogin(id: Int16, password: String) -> Bool {
        
        let employeeFetch: NSFetchRequest<User> = User.fetchRequest()
        let idPredicate       = NSPredicate(format: "%K == %d", #keyPath(User.id), id)
        let passwordPredicate = NSPredicate(format: "%K == %@", #keyPath(User.password), password)
        
        employeeFetch.predicate = NSCompoundPredicate(andPredicateWithSubpredicates: [idPredicate, passwordPredicate])
        do {
            let employeeResultsCount = try appDelegate!.stack.managedContext.count(for: employeeFetch)
            return employeeResultsCount > 0
        } catch let error as NSError {
            print("Fetch error: \(error) description: \(error.userInfo)")
        }
        return false
    }
    
}
